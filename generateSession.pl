#!/usr/bin/perl -w
#
# generate igv session.xml
#
use strict;
use Getopt::Std;
#


my %options=();
getopts("d:b:f:r:g:", \%options);


my $filesession;

my $stringSession ="";

my $dirBam=$options{d};	 #bam
my $fileRef=$options{r}; #fasta
my $fileGff=$options{f}; #gff plusieurs fichiers gff


my @tabbams = split(/\s+/,$options{b}); # plusieurs fichiers bam 

my $i;
$stringSession = qq |<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Session genome="$fileRef" hasGeneTrack="false" hasSequenceTrack="true" nextAutoscaleGroup="2" version="8">
|;

#add gff ressources
my @tabGff = split(/\s+/,$fileGff);




$stringSession .= "<Resources>\n";
for($i=0;$i<=$#tabbams;$i++){
	$stringSession .= qq|<Resource path="$dirBam/$tabbams[$i]"/>
|;
}
for($i=0;$i<=$#tabGff;$i++){
	$stringSession .=qq|<Resource name ="g$i.gff" path="$tabGff[$i]"/>
|;
}
$stringSession .= "</Resources>\n";

my $panel ="";
for($i=0;$i<=$#tabbams;$i++){
	$panel ="Pan".$i;
	$stringSession .= qq |
<Panel height="786" name="$panel" width="1901">
        <Track attributeKey="$tabbams[$i] Coverage" autoScale="true" clazz="org.broad.igv.sam.CoverageTrack" color="175,175,175" colorScale="ContinuousColorScale;0.0;169411.0;255,255,255;175,175,175" fontSize="10" id="$dirBam/$tabbams[$i]_coverage" name="$tabbams[$i] Coverage" snpThreshold="0.2" visible="true">
            <DataRange baseline="0.0" drawBaseline="true" flipAxis="false" maximum="126399.0" minimum="0.0" type="LOG"/>
        </Track>
        <Track attributeKey="$tabbams[$i] Junctions" clazz="org.broad.igv.sam.SpliceJunctionTrack" fontSize="10" height="60" id="$dirBam/$tabbams[$i]_junctions" name="$tabbams[$i] Junctions" visible="false"/>
        <Track attributeKey="$tabbams[$i]" clazz="org.broad.igv.sam.AlignmentTrack" displayMode="SQUISHED" experimentType="OTHER" fontSize="10" id="$dirBam/$tabbams[$i]" name="$tabbams[$i]" visible="true">
            <RenderOptions quickConsensusMode="true"/>
        </Track>
    </Panel>
|;
}
$stringSession .=qq|<Panel height="20" name="FeaturePanel" width="1901">
|;
for($i=0;$i<=$#tabGff;$i++){
	$stringSession .=qq|
<Track attributeKey="g$i.gff" clazz="org.broad.igv.track.FeatureTrack" color="0,0,178" displayMode="EXPANDED" fontSize="10" id="$tabGff[$i]" name="$i.gff" visible="true"/>
|;
}
$stringSession .=qq|</Panel>
|;

$stringSession .= qq|
<Panel height="20" name="FeaturePanel" width="1901">
        <Track attributeKey="Reference sequence" clazz="org.broad.igv.track.SequenceTrack" fontSize="10" id="Reference sequence" name="Reference sequence" visible="true"/>
    </Panel>
    <HiddenAttributes>
        <Attribute name="DATA FILE"/>
        <Attribute name="DATA TYPE"/>
        <Attribute name="NAME"/>
    </HiddenAttributes>|;

#@tabgenelist Id file
#  MN908947_Ampliseq:3208-3218
#   <Frame chr="MN908947_Ampliseq" end="3218.0" name="MN908947_Ampliseq:3208-3218" start="3207.0"/>

my @pre = split( /\./, $tabbams[0] );
$filesession = $pre[0];
if (defined $options{g}){
	#my @tabgenelist =split(/\s+/,$options{g});
	$filesession .=".xml";
	my $fileGeneList = $options{g};


	#print  FI   qq|<GeneList name="$tabgenelist[0]">|;
	open(GL, "$fileGeneList") || die "can't open $fileGeneList\n";
	while ( my $line = <GL> ) {
	    chomp $line;
	    my @array = split( "\t", $line );
		if ($array[0] !~ m/^#name/){
	    #if($array[0] eq $tabgenelist[0]){
			my $stringGeneSession = $stringSession;
			$stringGeneSession.=qq|
			<GeneList name="$array[0]">|;
			for($i=2;$i<=$#array;$i++){
				$stringGeneSession .= $array[$i]."\n";
			}
			for($i=2;$i<=$#array;$i++){
				my @tab = split( ":", $array[$i] );
				my @tabPos = split( "-", $tab[1] );
				$stringGeneSession .= qq|<Frame chr="$tab[0]" end="$tabPos[1].0" name="$array[$i]" start="$tabPos[0].0"/>
				|;
			}

			$stringGeneSession .= "\n</GeneList>\n";
			$stringGeneSession .= qq|
</Session>
|;	
			
			my $geneFile = $array[0]."_".$filesession;
			open(FI, ">$geneFile") || die "can't open $geneFile\n";
			print FI $stringGeneSession;
			close(FI);
	   # }
		}
	   	
	}
	close(GL);
	
}
else{
	$stringSession .= qq|
</Session>
|;
	
	$filesession .= "_full.xml";
	#$filesession = "_full.xml";
	open(FI, ">$filesession") || die "can't open $filesession\n";
	print FI $stringSession;
	close(FI);
}
